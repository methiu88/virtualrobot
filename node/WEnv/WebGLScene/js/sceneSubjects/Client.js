import * as THREE from '../../node_modules/three/build/three.module.js'

export default (scene, clientConfiguration) => {
    const clientPosition = { x: clientConfiguration.position.x, y: 2, z: clientConfiguration.position.y }

    const group = new THREE.Group()    
    group.position.set(clientPosition.x, clientPosition.y, clientPosition.z)
    scene.add(group)

    loadClientMeshAsync()

    function loadClientMeshAsync() {
    	var objectLoader = new THREE.ObjectLoader();
	objectLoader.load("models/sonic-the-hedgehog.json", function ( obj ) {
		// Mario
		//obj.scale.set( .025, .025, .025 );
		
		//Sonic
		obj.scale.set( .1, .1, .1 );
		obj.position.y = -1
		
		group.add( obj );
	} );
    }
    

    function update(time) {
    
    
	//const scale = (Math.sin(time)+4)/5
	const positionY = Math.sin(time)/4
        
	//group.scale.set( clientConfiguration.size.x, 1, clientConfiguration.size.y )
	group.position.set( clientConfiguration.position.x, 2, -clientConfiguration.position.y )

	group.position.y = clientPosition.y + positionY
    }

    return {
        mesh: group,
        update
    }
}
